import argparse
import sys
import re
import datetime
import operator
# Fixing random state for reproducibility
from prettytable import PrettyTable




import argparse
import sys
def count_data(count,user_id):
	if user_id not in count:
		count[user_id]=1
	else :
		count[user_id]+=1



def tuple_to_prettytable(turple):
	table=PrettyTable()
	table.field_names = ["user","count"]

	for index,item in enumerate(turple):
		table.add_row([ item[0]  ,item[1]   ] )

	print (table)
	return 0

def check_arg(args=None):
	parser = argparse.ArgumentParser(description='Script to learn basic argparse')


	parser.add_argument(	'filename' ,type=str, 
				help='Log file Path')



	parser.add_argument(	'-u', 
				help='Summary failed login log and sort log by user',
				action="store_true")

	parser.add_argument(	'-after',metavar="AFTER",
				help='filter log, after date. format YYYY-MM-DD-HH:MM:SS'
				)

	parser.add_argument(	'-before',metavar="BEFORE",
				help='filter log, before date. format YYYY-MM-DD-HH:MM:SS'
				)

	parser.add_argument(	'-n', metavar='N', type=int,
				help='Show only the user of most N-th times')


	parser.add_argument(	'-t', metavar='T', type=int,
				help='Show only the user of attacking equal or more than T times')



	parser.add_argument(	'-r',
				help='Sort in reverse order'
				,action='store_true')

	results = parser.parse_args(args)

	return (
			results.filename,
			results.u,
			results.after,
			results.before,
			results.n,
			results.t,
			results.r
			)

if __name__ == '__main__':


	month={
		"Jan":1,
		"Feb":2,
		"Mar":3,
		"Apr":4,
		"May":5,
		"Jun":6,
		"Jul":7,
		"Aug":8,
		"Sep":9,
		"Oct":10,
		"Nov":11,
		"Dec":12
	}

	log_file_name,user,after,before,n,t,r= check_arg(sys.argv[1:])
	'''
	print(log_file_name)
	print(user)
	print(after)
	print(before)
	print(n)
	print(t)
	print(r)
	'''
	time_after=None
	time_before=None

	if after!=None:
		match=re.search(r".*(\d{4})-(\d{2})-(\d{2})-(\d{2}):(\d{2}):(\d{2})*",str(after))
		'''
		print (match.group(1))
		print (match.group(2))
		print (match.group(3))
		print (match.group(4))
		print (match.group(5))
		print (match.group(6))
		'''
		time_after = datetime.datetime(int((match.group(1))) , int(match.group(2)) , int(match.group(3)) , int(match.group(4)) ,int( match.group(5)),int(match.group(6)) )


	if before!=None:
		match=re.search(r".*(\d{4})-(\d{2})-(\d{2})-(\d{2}):(\d{2}):(\d{2})*",str(before))
		'''
		print (match.group(1))
		print (match.group(2))
		print (match.group(3))
		print (match.group(4))
		print (match.group(5))
		print (match.group(6))
		'''
		time_before = datetime.datetime(int((match.group(1))) , int(match.group(2)) , int(match.group(3)) , int(match.group(4)) ,int( match.group(5)),int(match.group(6)) )

	count={}


#Mar  8 00:49:45 <auth.info> lxc sshd[3804]: Invalid user pi from 223.18.194.86 port 46762
#Mar  8 00:49:46 <auth.info> lxc sshd[3804]: Failed password for invalid user pi from 223.18.194.86 port 46762 ssh2

#Mar  8 09:50:21 linux7 sshd[15899]: Invalid user ubnt from 188.187.121.68 port 47664
#Mar  8 09:50:23 linux7 sshd[15899]: Failed password for invalid user ubnt from 188.187.121.68 port 47664 ssh2


	file_log=open(log_file_name,"r")
	#print("~~~~~~~~~~~~~~~~~")
	for line in file_log:
	
		match=re.search(r".*nvalid user (\w+).*",line)
		user_id=str(match.group(1))
#		print ("user: "+str(match.group(1)) )

		match=re.search(r"(\w+)  (\d+) (\d{2}):(\d{2}):(\d{2}).*",line)

		date_day = datetime.datetime(2018,month[(match.group(1))] , int(match.group(2)) , int(match.group(3)) , int(match.group(4)) ,int( match.group(5)) )
#		print (date_day)

		if after!=None and before!=None:
			if date_day>time_after and  date_day<time_before :
				count_data(count,user_id)

		elif after!=None:
			if date_day>time_after :
				count_data(count,user_id)

		elif before!=None:
			if date_day<time_before :
				count_data(count,user_id)

		else:
			count_data(count,user_id)



	#count = sorted(count.items(), key=operator.itemgetter(1))


		
#	turple=sorted(di.items(), key=lambda d: d[0]) #key
	turple=sorted(count.items(), key=lambda d: d[1],reverse=True)#value
#	print (turple)

	#for N
	if n!=None:
		turple=turple[0:n]
		#print(turple)

	#for T
	top_t=[]
	if t!=None:	
		for index,item in enumerate(turple):
			if t<=item[1]:
				top_t.append(item)
#	turple=top_t
	if user!=False:	
		turple=sorted(turple, key=lambda x: x[0])
#	print(turple)
	if r!=False:	
		turple.reverse()
#	print (turple)

	tuple_to_prettytable(turple)
